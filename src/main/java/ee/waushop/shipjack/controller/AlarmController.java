package ee.waushop.shipjack.controller;

import ee.waushop.shipjack.controller.ResourceNotFoundException;
import ee.waushop.shipjack.model.Alarm;
import ee.waushop.shipjack.repository.AlarmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AlarmController {

  @Autowired
  private AlarmRepository alarmRepository;

  @GetMapping("/alarms")
  public List<Alarm> getAllAlarms() {
    return alarmRepository.findAll();
  }

  @GetMapping("/alarms/{id}")
  public ResponseEntity<Alarm> getAlarmById(@PathVariable(value = "id") Long alarmId)
      throws ResourceNotFoundException {
    Alarm alarm =
    alarmRepository
            .findById(alarmId)
            .orElseThrow(() -> new ResourceNotFoundException("Alarm not found on :: " + alarmId));

    return ResponseEntity.ok().body(alarm);
  }

  @PostMapping("/alarms")
  public Alarm createAlarm(@Valid @RequestBody Alarm alarm) {
    return alarmRepository.save(alarm);
  }

  @PutMapping("/alarms/{id}")
  public ResponseEntity<Alarm> updateAlarm(
      @PathVariable(value = "id") Long alarmId, @Valid @RequestBody Alarm alarmDetails)
      throws ResourceNotFoundException {

    Alarm alarm =
    alarmRepository
            .findById(alarmId)
            .orElseThrow(() -> new ResourceNotFoundException("Alarm not found on :: " + alarmId));

    alarm.setAlarmNumber(alarmDetails.getAlarmNumber());
    alarm.setAlarmGroup(alarmDetails.getAlarmGroup());
    alarm.setLifeboat(alarmDetails.getLifeboat());
    final Alarm updatedAlarm = alarmRepository.save(alarm);
    return ResponseEntity.ok(updatedAlarm);
  }

  @DeleteMapping("/alarms/{id}")
  public Map<String, Boolean> deleteAlarm(@PathVariable(value = "id") Long alarmId) throws ResourceNotFoundException {
    Alarm alarm =
    alarmRepository
            .findById(alarmId)
            .orElseThrow(() -> new ResourceNotFoundException("Alarm not found on :: " + alarmId));

            alarmRepository.delete(alarm);
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }
}