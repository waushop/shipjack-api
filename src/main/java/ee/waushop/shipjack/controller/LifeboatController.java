package ee.waushop.shipjack.controller;

import ee.waushop.shipjack.controller.ResourceNotFoundException;
import ee.waushop.shipjack.model.Lifeboat;
import ee.waushop.shipjack.repository.LifeboatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class LifeboatController {

  @Autowired
  private LifeboatRepository lifeboatRepository;

  @GetMapping("/lifeboats")
  public List<Lifeboat> getAllLifeboats() {
    return lifeboatRepository.findAll();
  }

  @GetMapping("/lifeboats/{id}")
  public ResponseEntity<Lifeboat> getLifeboatById(@PathVariable(value = "id") Long lifeboatId)
      throws ResourceNotFoundException {
    Lifeboat lifeboat =
    lifeboatRepository
            .findById(lifeboatId)
            .orElseThrow(() -> new ResourceNotFoundException("Lifeboat not found on :: " + lifeboatId));

    return ResponseEntity.ok().body(lifeboat);
  }

  @PostMapping("/lifeboats")
  public Lifeboat createLifeboat(@Valid @RequestBody Lifeboat lifeboat) {
    return lifeboatRepository.save(lifeboat);
  }

  @PutMapping("/lifeboats/{id}")
  public ResponseEntity<Lifeboat> updateLifeboat(
      @PathVariable(value = "id") Long lifeboatId, @Valid @RequestBody Lifeboat lifeboatDetails)
      throws ResourceNotFoundException {

    Lifeboat lifeboat =
    lifeboatRepository
            .findById(lifeboatId)
            .orElseThrow(() -> new ResourceNotFoundException("Lifeboat not found on :: " + lifeboatId));

    lifeboat.setLifeboatName(lifeboatDetails.getLifeboatName());
    final Lifeboat updatedLifeboat = lifeboatRepository.save(lifeboat);
    return ResponseEntity.ok(updatedLifeboat);
  }

  @DeleteMapping("/lifeboats/{id}")
  public Map<String, Boolean> deleteLifeboat(@PathVariable(value = "id") Long lifeboatId) throws ResourceNotFoundException {
    Lifeboat lifeboat =
    lifeboatRepository
            .findById(lifeboatId)
            .orElseThrow(() -> new ResourceNotFoundException("Lifeboat not found on :: " + lifeboatId));

            lifeboatRepository.delete(lifeboat);
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }
}