package ee.waushop.shipjack.controller;

import ee.waushop.shipjack.controller.ResourceNotFoundException;
import ee.waushop.shipjack.model.AlarmGroup;
import ee.waushop.shipjack.repository.AlarmGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AlarmGroupController {

  @Autowired
  private AlarmGroupRepository alarmGroupRepository;

  @GetMapping("/alarmgroups")
  public List<AlarmGroup> getAllAlarmGroups() {
    return alarmGroupRepository.findAll();
  }

  @GetMapping("/alarmgroups/{id}")
  public ResponseEntity<AlarmGroup> getAlarmGroupById(@PathVariable(value = "id") Long alarmGroupId)
      throws ResourceNotFoundException {
    AlarmGroup alarmGroup =
    alarmGroupRepository
            .findById(alarmGroupId)
            .orElseThrow(() -> new ResourceNotFoundException("Alarm group not found on :: " + alarmGroupId));

    return ResponseEntity.ok().body(alarmGroup);
  }

  @PostMapping("/alarmgroups")
  public AlarmGroup createAlarmGroup(@Valid @RequestBody AlarmGroup alarmGroup) {
    return alarmGroupRepository.save(alarmGroup);
  }

  @PutMapping("/alarmgroups/{id}")
  public ResponseEntity<AlarmGroup> updateAlarmGroup(
      @PathVariable(value = "id") Long alarmGroupId, @Valid @RequestBody AlarmGroup alarmGroupDetails)
      throws ResourceNotFoundException {

    AlarmGroup alarmGroup =
    alarmGroupRepository
            .findById(alarmGroupId)
            .orElseThrow(() -> new ResourceNotFoundException("Alarm group not found on :: " + alarmGroupId));

    alarmGroup.setAlarmGroupName(alarmGroupDetails.getAlarmGroupName());
    final AlarmGroup updatedAlarmGroup = alarmGroupRepository.save(alarmGroup);
    return ResponseEntity.ok(updatedAlarmGroup);
  }

  @DeleteMapping("/alarmgroups/{id}")
  public Map<String, Boolean> deleteAlarmGroup(@PathVariable(value = "id") Long alarmGroupId) throws ResourceNotFoundException {
    AlarmGroup alarmGroup =
    alarmGroupRepository
            .findById(alarmGroupId)
            .orElseThrow(() -> new ResourceNotFoundException("Alarm group not found on :: " + alarmGroupId));

            alarmGroupRepository.delete(alarmGroup);
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }
}