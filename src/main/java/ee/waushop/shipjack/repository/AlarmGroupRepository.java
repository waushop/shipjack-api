package ee.waushop.shipjack.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ee.waushop.shipjack.model.AlarmGroup;

@Repository
public interface AlarmGroupRepository extends JpaRepository<AlarmGroup, Long>{

}