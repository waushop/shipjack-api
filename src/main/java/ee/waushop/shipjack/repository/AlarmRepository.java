package ee.waushop.shipjack.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ee.waushop.shipjack.model.Alarm;

@Repository
public interface AlarmRepository extends JpaRepository<Alarm, Long>{

}