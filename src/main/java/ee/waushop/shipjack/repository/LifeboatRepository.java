package ee.waushop.shipjack.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ee.waushop.shipjack.model.Lifeboat;

@Repository
public interface LifeboatRepository extends JpaRepository<Lifeboat, Long>{

}