package ee.waushop.shipjack.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="alarms")
public class Alarm {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="alarm_number")
	private String alarmNumber;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="alarm_group_id")
	private AlarmGroup alarmGroup;

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="lifeboat_id")
	private Lifeboat lifeboat;
	
	public Alarm() {
		
	}

	public Alarm(String alarmNumber, AlarmGroup alarmGroup, Lifeboat lifeboat) {
		this.alarmNumber = alarmNumber;
		this.alarmGroup = alarmGroup;
		this.lifeboat = lifeboat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlarmNumber() {
		return alarmNumber;
	}

	public void setAlarmNumber(String alarmNumber) {
		this.alarmNumber = alarmNumber;
	}

	public AlarmGroup getAlarmGroup() {
		return alarmGroup;
	}

	public void setAlarmGroup(AlarmGroup alarmGroup) {
		this.alarmGroup = alarmGroup;
	}

	public Lifeboat getLifeboat() {
		return lifeboat;
	}

	public void setLifeboat(Lifeboat lifeboat) {
		this.lifeboat = lifeboat;
	}

	@Override
	public String toString() {
		return "Alarm [id=" + id + ", alarmNumber=" + alarmNumber + ", alarmGroup=" + alarmGroup + ", lifeboat=" + lifeboat + "]";
	}
}