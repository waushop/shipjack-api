package ee.waushop.shipjack.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="alarm_groups")
public class AlarmGroup {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="alarm_group_name")
	private String alarmGroupName;
	
	public AlarmGroup() {
		
	}

	public AlarmGroup(String alarmGroupName) {
		this.alarmGroupName = alarmGroupName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlarmGroupName() {
		return alarmGroupName;
	}

	public void setAlarmGroupName(String alarmGroupName) {
		this.alarmGroupName = alarmGroupName;
	}

	@Override
	public String toString() {
		return "AlarmGroup [id=" + id + ", alarmGroupName=" + alarmGroupName + "]";
	}
}