package ee.waushop.shipjack.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lifeboats")
public class Lifeboat {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="lifeboat_name")
	private String lifeboatName;
	
	public Lifeboat() {
		
	}

	public Lifeboat(String lifeboatName) {
		this.lifeboatName = lifeboatName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLifeboatName() {
		return lifeboatName;
	}

	public void setLifeboatName(String lifeboatName) {
		this.lifeboatName = lifeboatName;
	}

	@Override
	public String toString() {
		return "Lifeboat [id=" + id + ", lifeboatName=" + lifeboatName + "]";
	}
}