package ee.waushop.shipjack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShipjackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShipjackApplication.class, args);
	}

}
